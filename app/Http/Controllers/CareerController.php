<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Career;
use App\Respond;
class CareerController extends Controller
{
    //

    public function ListCareer($lang){
        $arr = array();
        $objCareer = new Career();
        $arrCareer = $objCareer->list($lang);
        $arr['data'] = $arrCareer;
        $arr = Respond::mergeStatus($arr,200);
        return $arr;
    }
    public function GetCareerById($id,$lang){
        // dd($id . $lang);
        $arr = array();
        $objCareer = new Career();
        $arrCareer = $objCareer->getCareerById($lang,$id);
        $arr['data'] = $arrCareer;
        $arr = Respond::mergeStatus($arr,200);
        return $arr;
    }
}
