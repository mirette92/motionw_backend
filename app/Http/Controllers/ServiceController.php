<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Service;
use App\Respond;

class ServiceController extends Controller
{
    //

    public function ListServices($lang){
        
        $arr = array();
        $objService = new Service();
        $arrServices = $objService->list($lang);
        $arr['data'] = $arrServices;
        
        $arr = Respond::mergeStatus($arr,200);
        
        return $arr;
    }
}
