<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Section;
use App\Respond;

class SectionController extends Controller
{
    //
    public function ListSection($lang){
        $arr = array();
        $objSection = new Section();
        $arrSection = $objSection->list($lang);
        $arr['data'] = $arrSection;
        
        $arr = Respond::mergeStatus($arr,200);
        
        return $arr;
    }
}
