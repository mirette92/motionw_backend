<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\About;
use App\Respond;

class AboutController extends Controller
{
    //
    public function ListAbout($lang){
        
        $arr = array();
        $objAbout = new About();
        $arrAbout = $objAbout->list($lang);
        $arr['data'] = $arrAbout;
        
        $arr = Respond::mergeStatus($arr,200);
        
        return $arr;
    }
}
