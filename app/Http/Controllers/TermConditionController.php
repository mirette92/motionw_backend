<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Respond;
use App\TermCondition;

class TermConditionController extends Controller
{
    //
    public function ListTerm($lang){
        $arr = array();
        $objTerm = new TermCondition();
        $arrTerm = $objTerm->list($lang);
        $arr['data'] = $arrTerm;
        $arr = Respond::mergeStatus($arr,200);
        
        return $arr;
    }
}
