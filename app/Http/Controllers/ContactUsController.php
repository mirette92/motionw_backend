<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ContactUs;
use App\Respond;
use App\Helpers\mailerHelper;
class ContactUsController extends Controller
{
    //

    public function AddContactUs(Request $request){
        $arr = array();
        $objContactus = new ContactUs();
        if($request['user_email'] == null || $request['message'] == null){
            $arr = Respond::mergeStatus($arr,400);
            return $arr;
        }
        // dd($request);
        $objContactus->user_email = $request['user_email'];
        $objContactus->message = $request['message'];
        $objContactus->subject = $request['subject'];
        $objContactus->user_name = $request['user_name'];
        $result = $objContactus->save();
     
        $mail = mailerHelper::MailerSender("contactUs",$request['message'],$request['user_email'],$request['user_name'],$request['subject']);
     
        if($mail == true){
            $arr = Respond::mergeStatus($arr,200);
        }
       
        return $arr;
    
    }
}
