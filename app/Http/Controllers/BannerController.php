<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Banner;
use App\Respond;

class BannerController extends Controller
{
    //
    public function ListBanner($lang){
        
        $arr = array();
        $objBanner = new Banner();
        $arrBanner = $objBanner->list($lang);
        $arr['data'] = $arrBanner;
        
        $arr = Respond::mergeStatus($arr,200);
        
        return $arr;
    }
}
