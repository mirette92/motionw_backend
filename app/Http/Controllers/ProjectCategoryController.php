<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ProjectCategory;
use App\Respond;

class ProjectCategoryController extends Controller
{
    //
    public function ListProjectCategory(){
        $arr = array();
        $objProjectCategory = new ProjectCategory();
        $arrProjectCategory = $objProjectCategory->list();
        $arr['data'] = $arrProjectCategory;
        
        $arr = Respond::mergeStatus($arr,200);
        
        return $arr;
    }

    
}
