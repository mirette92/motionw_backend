<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Respond;
use App\PrivacyPolicy;

class PrivacyPolicyController extends Controller
{
    //

    public function ListPrivacy($lang){
        $arr = array();
        $objPrivacy = new PrivacyPolicy();
        $arrPrivacy = $objPrivacy->list($lang);
        $arr['data'] = $arrPrivacy;
        $arr = Respond::mergeStatus($arr,200);
        
        return $arr;
    }
}
