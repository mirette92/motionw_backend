<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\EmployeeProfile;
use App\Respond;

class EmployeeProfileController extends Controller
{
    //
    public function ListEmployee($lang){
        $arr = array();
        $objEmployee = new EmployeeProfile();
        $arrEmployee = $objEmployee->listEmployee($lang);
        $arr['data'] = $arrEmployee;
        $arr = Respond::mergeStatus($arr,200);
        return $arr;
    }
    
}
