<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContactUs extends Model
{
    //
    protected $table = 'contact_us';
    protected $fillable = [
        'id','user_email', 'message','created_at','updated_at'
    ];
     
}
