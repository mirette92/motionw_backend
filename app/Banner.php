<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;
use App\Helpers\translationHelper;

class Banner extends Model
{
    use Translatable;
    protected $table = 'banners';
    protected $fillable = [
        'id','title', 'sub_title','img','created_at','updated_at'
    ];
    protected $translatable  = ['title','sub_title'];

    public function list($lang){
        $arrBanner = $this->get();
        $arrBannerTrans = $arrBanner->translate($lang,'en');
        foreach($arrBannerTrans as $obj){
            // dd(env('APP_URL_Media'));
            if($obj['img'] != ''){
                $obj['img'] = env('APP_URL_Media').$obj['img'];
            }
            
        }
        $arrBanner2= translationHelper::translatedCollectionToArray($arrBannerTrans);
        
        return $arrBanner2;
    }
}
