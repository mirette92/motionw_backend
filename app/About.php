<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;
use App\Helpers\translationHelper;

class About extends Model
{
    use Translatable;
    protected $table = 'abouts';
    protected $fillable = [
        'id','title', 'icon','desc','created_at','updated_at'
    ];
    protected $translatable  = ['title','desc'];

    public function list($lang){
        $arrAbout = $this->get();
        $arrAboutTrans = $arrAbout->translate($lang,'en');
        foreach($arrAboutTrans as $obj){
            // dd(env('APP_URL_Media'));
            if($obj['icon'] != ''){
                $obj['icon'] = env('APP_URL_Media').$obj['icon'];
            }
            
        }
        $arrAbout2= translationHelper::translatedCollectionToArray($arrAboutTrans);
        
        return $arrAbout2;
    }
    
}
