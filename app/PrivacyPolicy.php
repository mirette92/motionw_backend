<?php

namespace App;
use App\Helpers\translationHelper;
use TCG\Voyager\Traits\Translatable;
use Illuminate\Database\Eloquent\Model;


class PrivacyPolicy extends Model
{
    use Translatable;
    protected $table = 'privacy_policies';
    protected $fillable = [
        'id','text'
    ];
    protected $translatable  = ['text'];

    public function list($lang){
        $arrPrivacy = $this->get();
        $arrPrivacyTrans = $arrPrivacy->translate($lang,'en');

        $arrPrivacy2 = translationHelper::translatedCollectionToArray($arrPrivacyTrans);
        return $arrPrivacy2;
    }
}
