<?php

namespace App;

use App\Helpers\translationHelper;
use TCG\Voyager\Traits\Translatable;
use Illuminate\Database\Eloquent\Model;

class EmployeeProfile extends Model
{
    //
    use Translatable;
    protected $table = 'employee_profiles';
    protected $fillable = [
        'id','name', 'title','img','created_at','updated_at'
    ];
    protected $translatable  = ['name','title'];

    public function listEmployee($lang){
        $arrEmployee = $this->get();
        $arrEmployeeTrans = $arrEmployee->translate($lang,'en');
        foreach($arrEmployeeTrans as $obj){
            if($obj['img'] != ''){
                $obj['img'] = env('APP_URL_Media').$obj['img'];
            }
            
        }
        $arrEmployee2= translationHelper::translatedCollectionToArray($arrEmployeeTrans);
        
        return $arrEmployee2;
    }
}
