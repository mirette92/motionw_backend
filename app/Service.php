<?php

namespace App;

use App\Helpers\translationHelper;
use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;

class Service extends Model
{
    use Translatable;
    protected $table = 'services';
    protected $fillable = [
        'id','name', 'icon','created_at','updated_at'
    ];
    protected $translatable  = ['name'];

    public function list($lang){
        $arrServices = $this->get();
        $arrServicesTrans = $arrServices->translate($lang,'en');
        foreach($arrServicesTrans as $obj){
            // dd(env('APP_URL_Media'));
            if($obj['icon'] != ''){
                $obj['icon'] = env('APP_URL_Media').$obj['icon'];
            }
            
        }
        $arrServices2= translationHelper::translatedCollectionToArray($arrServicesTrans);
        
        return $arrServices2;
    }
    
}
