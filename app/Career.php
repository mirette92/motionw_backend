<?php

namespace App;

use App\Helpers\translationHelper;
use TCG\Voyager\Traits\Translatable;
use Illuminate\Database\Eloquent\Model;


class Career extends Model
{
    use Translatable;
    protected $table = 'careers';
    protected $fillable = [
        'id','title', 'desc','created_at','updated_at'
    ];
    protected $translatable  = ['title','desc'];

    public function list($lang){
        $arrCarrer = $this->get();
        $arrCareerTrans = $arrCarrer->translate($lang,'en');
        $arrCarrer2 = translationHelper::translatedCollectionToArray($arrCareerTrans);
        return $arrCarrer2;
    }
    public function getCareerById($lang,$id){
        $arrCareer = $this->where('id',$id)->get();
        $arrCareerTrans = $arrCareer->translate($lang,'en');
        $arrCarrer2 = translationHelper::translatedCollectionToArray($arrCareerTrans);
        return $arrCarrer2;
    }


}
