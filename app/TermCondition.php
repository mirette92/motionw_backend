<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\translationHelper;
use TCG\Voyager\Traits\Translatable;

class TermCondition extends Model
{
    use Translatable;
    protected $table = 'term_conditions';
    protected $fillable = [
        'id','text'
    ];
    protected $translatable  = ['text'];

    public function list($lang){
        $arrTerm = $this->get();
        $arrTermTrans = $arrTerm->translate($lang,'en');

        $arrTerm2 = translationHelper::translatedCollectionToArray($arrTermTrans);
        return $arrTerm2;
    }
}
