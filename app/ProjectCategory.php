<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectCategory extends Model
{
    //
    protected $table = 'projectCategories';
    protected $fillable = [
        'id','name','created_at','updated_at'
    ];
    public function list(){
        $arrProjectCategory = $this->with('project')->get();
        // dd($arrProjectCategory);
        return $arrProjectCategory;
    }

    public function project()
    {
        return $this->hasMany('App\project');
    }
}
