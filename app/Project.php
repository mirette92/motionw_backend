<?php

namespace App;

use App\Helpers\translationHelper;
use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;

class Project extends Model
{
    use Translatable;
    protected $table = 'projects';
    protected $fillable = [
        'id','name','img','desc','section_id','created_at','updated_at','multi_media'
    ];
    protected $translatable = ['name','desc'];

    public function getProjectbyId($id,$lang){
        $objProject = $this->where('id',$id)->get();
        $objProjectTrans = $objProject->translate($lang,'en');
        foreach($objProjectTrans as $obj){
            if($obj['img'] != ''){
                $obj['img'] = env('APP_URL_Media').$obj['img'];
            }
            $data = json_decode($obj['multi_media']);
            $test = array();
            if(!empty($data)){
                foreach($data as $index=>$media){
                    // dd(env('APP_URL_Media').$media->download_link);
                    $pos = strrpos( $media->download_link, ".");
                    $c = substr( $media->download_link, $pos);
                    $imgExts = array(".gif", ".jpg", ".jpeg", ".png", ".tiff", ".tif"); // this is far from complete
                    
                    if (in_array($c, $imgExts)){
                            $test[$index]['url'] = env('APP_URL_Media').$media->download_link;
                            $test[$index]['type'] = 'img';
                    }else{
                        $test[$index]['url'] = env('APP_URL_Media').$media->download_link;
                        $test[$index]['type'] = 'video';
                    }
                    // $test[] = env('APP_URL_Media').$media->download_link;
                }
            }
            $obj['multi_media'] = $test;
            $objProDetails = new ProjectDetail();
            $obj['projectDetails'] = $objProDetails->getdetilsAttacgedtoProject($id,$lang);
            
          
        }
        $objProject2= translationHelper::translatedCollectionToArray($objProjectTrans);

        return $objProject2;
    }
    public function getProjectAttachedtoSection($id,$lang){
        $arrProject = $this->where('section_id',$id)->get();
        $arrProjectTrans = $arrProject->translate($lang,'en');
        foreach($arrProjectTrans as $obj){
            $obj['img'] = env('APP_URL_Media').$obj['img'];
        }
        $arrProject2 = translationHelper::translatedCollectionToArray($arrProjectTrans);
        return $arrProject2;
    }
}
