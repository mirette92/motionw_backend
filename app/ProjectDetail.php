<?php

namespace App;

use App\Helpers\translationHelper;
use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;

class ProjectDetail extends Model
{
    use Translatable;
    protected $table = 'project_details';
    protected $fillable = [
        'id','title','media','desc','project_id','created_at','updated_at','rank'
    ];
    protected $translatable  = ['title','desc'];

    public function getdetilsAttacgedtoProject($project_id,$lang){
        $arrProjectDetails = $this->where('project_id',$project_id)->orderBy('rank','ASC')->get();
        
        $arrProjectDetailsTrans = $arrProjectDetails->translate($lang,'en');
        foreach($arrProjectDetailsTrans as $obj){
            // dd(json_decode($obj['media']));
            $data =json_decode($obj['media']);
            // dd($data);
            $test = array();
            if(!empty($data)){
                $d = $data[0];

                $pos = strrpos( $d->download_link, ".");
                $c = substr( $d->download_link, $pos);
                $imgExts = array(".gif", ".jpg", ".jpeg", ".png", ".tiff", ".tif"); // this is far from complete
                
                if (in_array($c, $imgExts)){
                        $test['url'] = env('APP_URL_Media').$d->download_link;
                        $test['type'] = 'img';
                }else{
                    $test[$index]['url'] = env('APP_URL_Media').$d->download_link;
                    $test[$index]['type'] = 'video';
                }


            // dd($d->download_link);
            
                // $obj['media'] = env('APP_URL_Media').$d->download_link;
            }
            $obj['media'] = $test;
        }
        $arrProjectDetails2= translationHelper::translatedCollectionToArray($arrProjectDetailsTrans);
        
        return $arrProjectDetails2;
    }

}
