<?php

namespace App;

use App\Helpers\translationHelper;
use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;
use App\Project;
class Section extends Model
{
    use Translatable;
    protected $table = 'sections';
    protected $fillable = [
        'id','name','created_at','updated_at'
    ];
    protected $translatable = ['name'];

    public function list($lang){
        $arrProjectCategory = $this->with('project')->get();
        $arrProjectCategoryTrans = $arrProjectCategory->translate($lang,'en');
        foreach($arrProjectCategoryTrans as $obj){
            $objProject = new Project();

            $obj['project'] = $objProject->getProjectAttachedtoSection($obj['id'],$lang);
        }
        $arrProjectCategory2= translationHelper::translatedCollectionToArray($arrProjectCategoryTrans);
        // dd($arrProjectCategory);
        return $arrProjectCategory2;
    }
    public static function listSection($lang){
        $arrProjectCategory = Section::get();
        // dd($arrProjectCategory);
        // $arrProjectCategoryTrans = $arrProjectCategory->translate($lang,'en');
        // foreach($arrProjectCategoryTrans as $obj){
        //     $objProject = new Project();

        //     $obj['project'] = $objProject->getProjectAttachedtoSection($obj['id'],$lang);
        // }
        // $arrProjectCategory2= translationHelper::translatedCollectionToArray($arrProjectCategoryTrans);
        // dd($arrProjectCategory2);
        return $arrProjectCategory;
    }
    public function project()
    {
        return $this->hasMany('App\Project', 'section_id', 'id');
    }
}
