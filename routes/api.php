<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/contact_us','ContactUsController@AddContactUs');

Route::get('/listServices/{lang}','ServiceController@ListServices');

Route::get('/listSection/{lang}','SectionController@ListSection');
Route::get('/getProjectById/{project_id}/{lang}','ProjectController@GetProjectById');

Route::get('/listCareer/{lang}','CareerController@ListCareer');

Route::get('/getCareerById/{career_id}/{lang}','CareerController@GetCareerById');

Route::get('/listEmployee/{lang}','EmployeeProfileController@ListEmployee');
Route::get('/getPrivacy/{lang}','PrivacyPolicyController@ListPrivacy');
Route::get('/getTerm/{lang}','TermConditionController@ListTerm');
Route::get('/listAbout/{lang}','AboutController@ListAbout');
Route::get('/listBanner/{lang}','BannerController@ListBanner');