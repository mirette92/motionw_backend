var app = angular.module('myApp', ['ngFileUpload']);
app.directive('file', function () {
    return {
        scope: {
            file: '='
        },
        link: function (scope, el, attrs) {
            el.bind('change', function (event) {
                var file = event.target.files[0];
                scope.file = file ? file : undefined;
                scope.$apply();
            });
        }
    };
});
app.controller('myCtrl', function($scope, Upload) {

    $scope.objProjectDesc = {title:'',position:'',data:'',desc:''};
    $scope.arrProjectDesc = [];
    $scope.lang = 'en';
    $scope.files = [];
    $scope.init = function(){
       $scope.addProjectDesc();
    }
    $scope.addProjectDesc = function(){
        $scope.arrProjectDesc.push(angular.copy($scope.objProjectDesc));

    }
    $scope.deleteProject = function(index){
        $scope.arrProjectDesc.splice(index,1)
    }

    $scope.uploadS = function () {
        $http({
            method: 'POST',
            url: '/upload-file',
            headers: {
                'Content-Type': 'multipart/form-data'
            },
            data: {
                email: Utils.getUserInfo().email,
                token: Utils.getUserInfo().token,
                upload: $scope.file
            },
            transformRequest: function (data, headersGetter) {
                var formData = new FormData();
                angular.forEach(data, function (value, key) {
                    formData.append(key, value);
                });

                var headers = headersGetter();
                delete headers['Content-Type'];

                return formData;
            }
        })
        .success(function (data) {

        })
        .error(function (data, status) {

        });
    };
    $scope.submit = function(index) {
        $('#collect').val(JSON.stringify($scope.arrProjectDesc));
        if ($scope.arrProjectDesc[index].data.$valid && $scope.arrProjectDesc[index].data) {
          $scope.upload($scope.arrProjectDesc[index].data);
        }
      };
    

      
    $scope.local = function(lang){
        $scope.lang = lang;
    }
   
    $scope.check = function(){
        $('#collect').val(JSON.stringify($scope.arrProjectDesc));
    }
    
    
    
    $scope.init();


    
});


